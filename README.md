ROIPipeline
===========

Demonstrates a concurrent Region Of Interest (ROI) detection pipeline written with the [Myriad Data Reduction Framework](https://emphysic.com/myriad/), analogous to the [single-process PipelineDemo example](https://emphysic.com/myriad/sample-code/roi-detection-pipeline/).
   
What It Does
============
A bundled machine learning model based on the [Passive Aggressive algorithm](http://www.jmlr.org/papers/v7/crammer06a.html) was trained to recognize possible indications of structural damage from [ultrasonic C-scan](http://www.olympus-ims.com/en/ndt-tutorials/instrumententation/cscan/) data.  More details on the training process is available from the [Myriad Trainer documentation](http://myrdocs.azurewebsites.net/trainer/#inital-preparation).
  
A concurrent processing pipeline is constructed in which each input file is read and subsets of data are passed to the defect-detection model.  The model examines the data and reports whether or not it found an indication of damage in the data.

How To Use It
=============
Follow the [Myriad installation instructions](http://myrdocs.azurewebsites.net/install/) to get Myriad installed on your machine.  You'll need to have [Java 8](http://java.sun.com/) and [Apache Maven](http://maven.apache.org/) installed.  Myriad has been tested to run on Windows 7, 10, various 32 and 64 bit flavors of Linux, FreeBSD, TrueOS, and OpenBSD.

Once Myriad has successfully been installed, open a command prompt in the Myriad Demo root folder (the folder with the *pom.xml* file).  Run Maven to build:
 
<pre>mvn package</pre>

After a few minutes the file *MyriadDemo-1.0-SNAPSHOT-allinone.jar* should be created in the *target/* folder.  Run this JAR and specify any files you'd like to analyze at the command line with e.g.

<pre>java -jar MyriadDemo-1.0-SNAPSHOT-allinone.jar /home/username/MyriadDemo/samples/140.csv</pre>

We've provided some sample input files under the *samples/* subfolder you can use to get started.  Press CTRL-C to end the demonstration.

How It Works
============
This application makes use of several varieties of Myriad's [LinkedWorkerPool](http://myrdocs.azurewebsites.net/api/com/emphysic/myriad/network/LinkedWorkerPool.html).  A LinkedWorkerPool is a pool of workers under the management of a single router.  When the router receives a work order it relays the work to the next available worker.  When the worker completes their task it is sent back to the router, which then sends it on to the next LinkedWorkerPool.  If a worker fails to complete the work, the work is resubmitted to the router which then relays to the next worker (restarting workers if necessary).  

The ROI detection pipeline is constructed in which input datafiles are read and scanned for ROI in a six-step process:

1.  The input file(s) are read by an [Ingestion pool](http://myrdocs.azurewebsites.net/api/com/emphysic/myriad/network/DataIngestorPool.html).  Currently-supported file formats include:  
    * [Delimited text](http://myrdocs.azurewebsites.net/api/com/emphysic/myriad/core/data/io/TextDataset.html);
    * [Various image formats](http://myrdocs.azurewebsites.net/api/com/emphysic/myriad/core/data/io/ImageDataset.html) including GIF, PNG, JPEG, and TIFF; and 
    * [DICOM/DICONDE](http://myrdocs.azurewebsites.net/api/com/emphysic/myriad/core/data/io/DicomDataset.html) files.
2.  The Ingestion pool sends input file contents to a [Pyramid pool](http://myrdocs.azurewebsites.net/api/com/emphysic/myriad/network/PyramidActorPool.html) which blurs and subsamples the input to allow the ROI detector to consider the input at several different scales.
3.  The Pyramid pool sends each resized version of each input file to a [Preprocessing pool](http://myrdocs.azurewebsites.net/api/com/emphysic/myriad/network/DatasetOperationPool.html), which performs any preprocessing operations required for the ROI detection model.  In this case, a [Sobel filter](http://myrdocs.azurewebsites.net/api/com/emphysic/myriad/core/data/ops/SobelOperation.html) is applied to incoming data.
4.  The Sobel pool sends the filtered data to a [Sliding Window pool](http://myrdocs.azurewebsites.net/api/com/emphysic/myriad/network/SlidingWindowPool.html), which scans over the inputs and extracts subsets for the ROI detector to examine.
5.  For each window of data extracted by the Sliding Window pool, an [ROI Detection pool](http://myrdocs.azurewebsites.net/api/com/emphysic/myriad/network/ROIFinderPool.html) scans for ROI.  In this case, a [machine learning model](http://myrdocs.azurewebsites.net/api/com/emphysic/myriad/core/data/roi/MLROIFinder.html) was trained to detect indications of structural damage in ultrasonic sensor data.  
6.  Flaw detection workers in the ROI Detection pool report their findings to the [Reporting pool](http://myrdocs.azurewebsites.net/api/com/emphysic/myriad/network/ReporterActorPool.html).  In this case, the Reporting pool simply logs a message indicating whether or not a flaw signal was found.  If a flaw signal was found, it includes sufficient metadata such that the relative position of the flaw can be found in the original input data from Step 1.  

After each stage in the pipeline is constructed, the stages are connected by sending each stage a message with a [reference](http://doc.akka.io/docs/akka/2.4/general/actors.html#Actor_Reference) to the next stage.  This sets the stage's [next](http://myrdocs.azurewebsites.net/api/com/emphysic/myriad/network/LinkedWorkerPool.html#next) field - when the stage receives a completed task from one of its workers the results are automatically sent to the Akka actor referenced by *next*.  Each of the six stages runs concurrently and independently - if a stage is busy the previous stage simply posts its messages to the stage's mailbox and continues with its own work.

Files specified on the command line are read and processed directly.  The application then continues to run, waiting for new data to inspect, until the user terminates e.g. with CTRL-C.  If running the application on the server, sending a [FileMessage](http://myrdocs.azurewebsites.net/api/com/emphysic/myriad/network/messages/FileMessage.html) to Stage 1's data ingestion pool's Actor reference (e.g. *akka://ROIPipeline/user/IngestorPool#141020403*) will analyze the new data file.
 
Where To Go From Here
=====================
1. Try adjusting the number of workers in each pool and examining what effect the new configuration has on throughput.  
2. If you have access to [Trainer](http://myrdocs.azurewebsites.net/trainer/), try training the bundled model on your data to see if you get better results.
3. If you have access to [Desktop](http://myrdocs.azurewebsites.net/desktop/), try starting this server app on one machine and using Myriad's [remoting](http://doc.akka.io/docs/akka/snapshot/scala/remoting.html) feature to have Desktop call it for one or more stages of your data processing.
4. Have a look at [other code samples](https://emphysic.com/myriad/sample-code/) and the current [Myriad API](http://myrdocs.azurewebsites.net/api/) and get coding!

License
=======
Myriad, Myriad Trainer, Myriad Desktop, and this demonstration of Myriad are all licensed under the [Apache 2.0 license](https://www.apache.org/licenses/LICENSE-2.0).